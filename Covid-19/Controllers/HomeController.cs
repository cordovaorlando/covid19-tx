﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace Covid19.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            var fileName = "TexasCOVID19CaseCountData" + DateTime.Now.ToString("yyyyMMddHHmmssffff").ToString() + ".xlsx";
            fileName = fileName.Replace("/", "-");
            var filePath = "";
            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;



                using (WebClient client = new WebClient())
            {
                client.DownloadFile("https://www.dshs.state.tx.us/coronavirus/TexasCOVID19CaseCountData.xlsx",
                                    AppDomain.CurrentDomain.BaseDirectory + "/Covid-19_Data/" + fileName);
                filePath = "Covid-19_Data/" + fileName;
            }

           

            ViewData["FilePath"] = filePath;


            return View();
        }
    }
}
